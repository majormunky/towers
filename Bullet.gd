extends Area2D

var speed = 1000.0
var velocity = Vector2()

func start(pos: Vector2, dir: float):
	position = pos
	rotation = dir
	velocity = Vector2(speed, 0).rotated(dir)

func _physics_process(delta: float) -> void:
	position += velocity * delta

func _ready() -> void:
	pass

func _on_Bullet_body_entered(body: Node) -> void:
	if body.name == "EnemySoldier":
		queue_free()
