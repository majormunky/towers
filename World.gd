extends Node2D

enum {
	NORMAL,
	PLACE_TOWER
}

var state = NORMAL
var cell_size = 64
onready var create_tower_button = $Control/ColorRect/CreateTowerButton
onready var tilemap = $TileMap
onready var highlight = $HighlightTile
onready var Tower = preload("res://Tower.tscn")
onready var tower_group = $Towers
onready var money_label = $Control/ColorRect/MoneyLabel

func _ready() -> void:
	highlight.visible = false
	PlayerData.money = 1000
	money_label.bbcode_text = "Money: $" + str(PlayerData.money)
	set_process_input(true)

func _input(event: InputEvent) -> void:
	var pos = Vector2(
		(int(get_global_mouse_position().x/cell_size) * cell_size) + cell_size / 2,
		(int(get_global_mouse_position().y/cell_size) * cell_size) + cell_size / 2
	)
	if state == PLACE_TOWER:
		var cell_index = Vector2(
			int(get_global_mouse_position().x/cell_size),
			int(get_global_mouse_position().y/cell_size)
		)
		var tile_type = tilemap.get_cell(cell_index.x, cell_index.y)
		if tile_type != 1:
			highlight.visible = true
			highlight.position = pos
		else:
			highlight.visible = false
			return
		if event.is_action_type() && event.is_action_pressed("click"):
			create_tower(pos)
			state = NORMAL
			highlight.visible = false
			highlight.position = Vector2(-100, -100)
			create_tower_button.visible = true

func _on_CreateTowerButton_button_up() -> void:
	state = PLACE_TOWER
	highlight.visible = true
	create_tower_button.visible = false

func create_tower(pos: Vector2) -> void:
	if spend_money(400):
		var tower = Tower.instance()
		tower.position = pos
		tower_group.add_child(tower)

func spend_money(amount: int) -> bool:
	if PlayerData.money - amount > 0:
		PlayerData.money -= amount
		money_label.bbcode_text = "Money: $" + str(PlayerData.money)
		return true
	else:
		return false
