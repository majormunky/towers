extends Node2D

onready var Enemy = preload("res://EnemySoldier.tscn")
var total_enemies = 3
var enemies_spawned = 0

func _on_Timer_timeout() -> void:
	if enemies_spawned < total_enemies:
		var enemy = Enemy.instance()
		enemy.position = position
		get_parent().add_child(enemy)
		enemies_spawned += 1
