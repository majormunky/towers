extends KinematicBody2D

enum {
	IDLE,
	ATTACK,
	RETURN_TO_IDLE,
}

onready var turret_sprite = $TurretSprite
onready var raycast = $TurretSprite/RayCast2D
onready var shoot_timer = $ShootTimer
onready var muzzle_flash = $TurretSprite/MuzzleFlash

var default_rotation = 0.0
var state = IDLE
var Bullet = preload("res://Bullet.tscn")
var can_shoot = true
var target = null

func _ready() -> void:
	turret_sprite.rotation_degrees = default_rotation

func _physics_process(delta: float) -> void:
	update()
	match state:
		IDLE:
			pass
		ATTACK:
			if target:
				turret_sprite.rotation = (target.position - position).angle() + deg2rad(-90.0)
				if can_shoot:
					shoot(target.position)
		RETURN_TO_IDLE:
			turret_sprite.rotation_degrees = lerp(turret_sprite.rotation_degrees, default_rotation, 1.0)

func shoot(pos) -> void:
	var b = Bullet.instance()
	var angle = (pos - global_position).angle()
	b.start(global_position, angle + rand_range(-0.1, 0.1))
	get_parent().add_child(b)
	can_shoot = false
	shoot_timer.start()
	muzzle_flash.visible = true
	yield(get_tree().create_timer(0.1), "timeout")
	muzzle_flash.visible = false

func _on_EnemyDetector_body_entered(body: Node) -> void:
	if body.name == "EnemySoldier":
		print("Enemy detected")
		target = body
		if state == IDLE:
			state = ATTACK

func _on_EnemyDetector_body_exited(body: Node) -> void:
	if body.name == "EnemySoldier":
		print("Enemy Left Area")
		if target == body:
			target = null
			if state == ATTACK:
				state = RETURN_TO_IDLE

func _on_ShootTimer_timeout() -> void:
	can_shoot = true

