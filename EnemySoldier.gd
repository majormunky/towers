extends KinematicBody2D

var velocity = Vector2.RIGHT
var speed = 80.0

func _physics_process(delta: float) -> void:
	move_and_slide(velocity * speed)
